## Creating You Cypress project

Create project folder and name it whatever you want. Once you are in your project make sure you have done the following:

**Make sure that you have already run npm init or have a node_modules folder or package.json file in the root of your
project to ensure cypress is installed in the correct directory.**

While in your'e project folder run: **npm install cypress --save-dev** on your terminal. This will install Cypress locally as a dev dependency for your project.
You can now open cypress from the project root folder by running this: **node_modules/.bin/cypress open**

---

## Add a Test File

Create a new file in the cypress/integration folder that was created for you, name it whatever you like:

For detailed instructions on how to work with cypress visit **https://docs.cypress.io/guides/getting-started/installing-cypress.html#System-requirements**
Once you have a test file you can start writting your tests. Everytime you save the changes you've made to your test file, cypress will run automatically with the new changes.
Cypress needs to remain open for you to see it rerun your tests everytime you update your testscript.

---

## Clone this bitbucket Project

On your terminal run: **git clone https://simphiwes@bitbucket.org/twzar/asba_automation.git**.

## Running the tests

You can run either one of the following commands to open cypress: npx cypress open or yarn run cypress open. Once cypress is open you can got to the following folder cypress/integration/example_spec.js on the UI to run the tests. This will open a chrome window to run the automated tests. Unfortunately there is only support for chrome for now but cypress is working on adding more browsers.

If you prefer running the tests from the terminal and want to see the ui running the tests(headless) you can run the following command npx cypress run. This way of running the tests gives you a more comprehensive report of the test run. For more information got to https://docs.cypress.io/guides/guides/command-line.html#How-to-run-commands.